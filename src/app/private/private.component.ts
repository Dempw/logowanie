import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss']
})
export class PrivateComponent implements OnInit {

  constructor(private router: Router, private auth: AuthService, private location: Location) {
    this.location.go('');
   }

  ngOnInit() {
  }

  logout() {
    this.auth.logout();    
    this.router.navigate(['/login']);
  }

}
