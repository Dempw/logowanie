import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthService } from '../services/auth.service';

import { PrivateComponent } from './private.component';

describe( 'PrivateComponent', () => {
  let component: PrivateComponent;
  let fixture: ComponentFixture<PrivateComponent>;

  beforeEach( async(() => {
    let authServiceStub = {
      login: true,
      _isLogedIn: false,
      logout: true
    };

    TestBed.configureTestingModule( {
      declarations: [PrivateComponent],
      providers: [
        { provide: Router, useClass: RouterStub },
        { provide: AuthService, useValue: authServiceStub },
        { provide: Location, useClass: LocationStub }
      ]
    } )
      .compileComponents();
  } ) );

  beforeEach(() => {
    fixture = TestBed.createComponent( PrivateComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  } );

  it( 'should create', () => {
    expect( component ).toBeTruthy();
  } );
} );

class RouterStub {
  navigateByUrl( url: string ) { return url; }
}

class LocationStub {
  go( url: string ): void { }
}