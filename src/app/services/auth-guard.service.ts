import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';



@Injectable()
export class AuthGuard implements CanActivate {
  constructor( private router: Router, private auth: AuthService ) { }

  canActivate( route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): boolean {
    let url: string = state.url;

    return this.authorize( url );
  }

  private authorize( url: string ): boolean {
    if ( this.auth.isLogedIn ) {
      return true;
    }

    this.auth.redirectUrl = url;

    this.router.navigate( ['/login'] );
  }

}
