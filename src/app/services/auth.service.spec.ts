
import { AuthService } from './auth.service';
import { CredentialsDto } from '../models/credentials';

describe( 'AuthService', () => {
  let auth: AuthService;
  beforeEach(() => { auth = new AuthService() } );

  it( 'should verify credentials for test@test.pl Password1', done => {
    const credentials = new CredentialsDto( 'test@test.pl', 'Password1' );
    auth.login( credentials ).subscribe( authResponse => {
      expect( authResponse ).toBe( true );
      done();
    } );
  } );

  it( 'should not verify for mail wrong mail and Password1', done => {
    const credentials = new CredentialsDto( 'test@test2.pl', 'Password1' );
    auth.login( credentials ).subscribe( authResponse => {
      expect( authResponse ).toBe( false );
      done();
    } );
  } )

  it( 'should not verify for test@test.pl and wrong password', done => {
    const credentials = new CredentialsDto( 'test@test2.pl', 'Password2' );
    auth.login( credentials ).subscribe( authResponse => {
      expect( authResponse ).toBe( false );
      done();
    } );
  } )

  it( 'should have negative logout state after failed login atempt', done => {
    const credentials = new CredentialsDto( 'wrong@mail.pl', 'WrongPassword1' );
    auth.login( credentials ).subscribe(() => {
      expect( auth.isLogedIn ).toBe( false );
      done();
    } );
  } );

 it( 'should have positive state after successful login atempt', done => {
    const credentials = new CredentialsDto( 'test@test.pl', 'Password1' );
    auth.login( credentials ).subscribe(() => {
      expect( auth.isLogedIn ).toBe( true );
      done();
    } );
  } );

   it( 'should have logout state after calling logout method', done => {
    const credentials = new CredentialsDto( 'test@test.pl', 'Password1' );
    auth.login( credentials ).subscribe(() => {
      auth.logout()
      expect( auth.isLogedIn ).toBe( false );
      done();
    } );
  } );

} );

