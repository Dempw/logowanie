import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';

import { CredentialsDto } from '../models/credentials';


const CREDENTIALS: CredentialsDto[] = [
  { email: 'test@test.pl', password: 'Password1' }
]

@Injectable()
export class AuthService {

  private _isLoggedIn: boolean = false;
  private _redirectUrl: string;

  get isLogedIn(): boolean { return this._isLoggedIn || !!localStorage.getItem('isLoggedIn'); }
  get redirectUrl(): string { return this._redirectUrl; }
  set redirectUrl(url: string) { this._redirectUrl = url; }
  login(credentials: CredentialsDto, remember?: boolean): Observable<boolean> {
    const authStatus = this.verifyCredentials(credentials);

    return Observable.of(authStatus).do(val => {
      this._isLoggedIn = authStatus;
      if (remember) { localStorage.setItem('isLoggedIn', '' + authStatus) }
    });
  }

  logout(): void {
    this._isLoggedIn = false;
    localStorage.clear();
  }

  private verifyCredentials(credentials: CredentialsDto): boolean {
    let userMatch = CREDENTIALS.find(c => (c.email === credentials.email && c.password === credentials.password) )
    return !!userMatch
  }

}


