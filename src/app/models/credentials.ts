export class CredentialsDto {
    constructor(
        public email: string = '',
        public password: string = '' ) { }
}
