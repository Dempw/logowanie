import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PrivateComponent } from './private/private.component';
import { LoginComponent } from './login/login.component';

import { AuthGuard } from './services/auth-guard.service';
import {AuthService} from './services/auth.service';

const routes: Routes = [
  { path: 'private', component: PrivateComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/private', pathMatch: 'full' }
];



@NgModule( {
  declarations: [
    AppComponent,
    PrivateComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot( routes )
  ],
  providers: [AuthGuard, AuthService],
  bootstrap: [AppComponent]
} )
export class AppModule { }
