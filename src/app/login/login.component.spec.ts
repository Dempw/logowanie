import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { LoginComponent } from './login.component';
import { FormsModule } from '@angular/forms';
import {AuthService} from '../services/auth.service';

describe( 'LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach( async(() => {
    let authServiceStub = {
      login: true,
      _isLogedIn: false,
      logout: true
    };


    TestBed.configureTestingModule( {
      declarations: [LoginComponent],
      imports: [FormsModule],
      providers: [
        { provide: Router, useClass: RouterStub },
        {provide: AuthService, useValue: authServiceStub}
      ]
    } )
      .compileComponents();
  } ) );

  beforeEach(() => {
    fixture = TestBed.createComponent( LoginComponent );
    component = fixture.componentInstance;
    fixture.detectChanges();
  } );

  it( 'should create', () => {
    expect( component ).toBeTruthy();
  } );
} );


class RouterStub {
  navigateByUrl( url: string ) { return url; }
}