import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';

import { CredentialsDto } from '../models/credentials';


@Component( {
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
} )
export class LoginComponent implements OnInit {

  credentials: CredentialsDto;
  rememberSession: boolean = false;
  emailConstraint: RegExp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  passwordConstraint: RegExp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$/;
  authFailed: boolean = false;


  ngOnInit(): void {
  }

  constructor( private router: Router, private auth: AuthService ) {
    this.credentials = new CredentialsDto();
  }

  login() {
    this.auth.login( this.credentials, this.rememberSession ).subscribe(() => {
      if ( this.auth.isLogedIn ) {
        const redirectUrl = this.auth.redirectUrl || '/private';
        this.router.navigate( [redirectUrl] );
      }
      else { this.authFailed = true; }
    } );
  }


  logout() {
    this.auth.logout();
  }

}
